const express = require('express');
const router = express.Router();
const path = require('path');

module.exports = ({ TasksService }) => {
  const tasks = [];

  router.get('/', (req, res) => {
    const error = req.query.error;
    const body = '';
    const success = req.query.success;
    res.render('index', { tasks, error, body, success });
  });

  router.post('/', (req, res) => {
    const task = req.body.task;

    if (task.length < 3) {
      return res.redirect('/?error=Minimal length for task name is 3 letters!');
    }

    if (tasks.some((t) => t.name === task)) {
      return res.redirect(`/error=Task "${task}" already exists!`);
    }

    tasks.push({ name: task, done: false });

    res.redirect('/?success=Task was added!');
  });

  router.get('/done', (req, res) => {
    const doneTasks = tasks.filter((task) => task.done);
    res.render('pages/done', { tasks: doneTasks });
  });

  router.post('/api/tasks/:taskName/done', (req, res) => {
    const taskName = req.params.taskName;
    const isChecked = req.body.done;
    const task = tasks.find((t) => t.name === taskName);

    if (!task) {
      return res.status(404).json({ error: `Task "${taskName}" not found.` });
    }

    task.done = isChecked;
    res.json({ message: `Task "${taskName}" marked as ${isChecked ? 'done' : 'undone'}.` });
  });

  return router;
};
