const express = require('express');
const makeStoppable = require('stoppable');
const http = require('http');
const routes = require('./routes');
const path = require('path');

const app = express();
const server = makeStoppable(http.createServer(app));

const TasksService = require('./services/tasks');

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    });
  };

  app.use(express.urlencoded({ extended: true }));

  app.set('view engine', 'ejs');
  app.set('views', path.join(__dirname, 'views'));
  app.use(express.static(path.join(__dirname, 'views')));

  app.use('/', routes({ TasksService }));

  app.use((req, res, next) => {
    res.locals.body = '';
    next();
  });
  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
};
