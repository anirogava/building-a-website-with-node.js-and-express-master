const tasks = [];

const addTask = (task) => {
  tasks.push(task);
};

const getAllTasks = () => {
  return tasks;
};

const markTaskAsDone = (taskName) => {
  const task = tasks.find((t) => t.name === taskName);
  if (task) {
    task.done = true;
  }
};

module.exports = {
  addTask,
  getAllTasks,
  markTaskAsDone
};
